export interface Note {
    id: number;
    title: string;
    content: string;
    user_id: string;
    created_at: Date | string;
    updated_at?: string | null;
    deleted_at?: boolean;
  }
  