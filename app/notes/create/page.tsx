'use client';

import Form from '@/components/Form'
import axios from 'axios';

const CreateNote = () => {

    const handleCreateNote = async (title: string, content: string) => {
        try {
            axios.post(`/api/notes`, {
                title,
                content
            });
        } catch (error) {
            console.error("Error creating note:", error);
        }
    }
    return (
        <div className="pt-10 sm:pt-20 sm:px-[16px] px-4 w-full max-w-full lg:max-w-[82rem] mx-auto">
            <h1 className="text-center text-3xl font-bold dark:text-white">Create Note</h1>
            <Form submit={handleCreateNote} />
        </div>
    )
}

export default CreateNote