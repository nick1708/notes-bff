'use client';

import Form from '@/components/Form'
import axios from 'axios';
import { useParams } from 'next/navigation';

const EditNote = () => {
    const params = useParams<{ id: string; }>();

    const handleEditNote = async (title: string, content: string) => {
        try {
            await axios.patch(`/api/note/${params.id}`, {
                title,
                content
            });
        } catch (error) {
            console.error("Error creating note:", error);
        }
    }
    return (
        <div className="pt-10 sm:pt-20 sm:px-[16px] px-4 w-full max-w-full lg:max-w-[82rem] mx-auto">
            <h1 className="text-center text-3xl font-bold dark:text-white">Edit Note</h1>
            <Form
                submit={handleEditNote}
                id={params.id || ''}
            />
        </div>
    )
}

export default EditNote