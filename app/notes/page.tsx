'use client';

import Table from "@/components/Table";
import { headsTable } from "@/data/notesHeads";
import { Note } from "@/interfaces/INotes";
import axios from "axios";
import Link from "next/link";
import { useEffect, useState } from "react";

const Notes = () => {
    const [notes, setNotes] = useState<Note[]>([]);

    const fetchData = async () => {
        const {
            data,
        } = await axios.get("/api/notes");

        setNotes(data);
    };

    const handleDelete = async (id: number) => {
        await axios.delete(`/api/note/${id}`);
        fetchData();
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="pt-10 sm:pt-20 sm:px-[16px] px-4 w-full max-w-full lg:max-w-[82rem] mx-auto">
            <Link href="/notes/create" className="block ml-auto mb-4 sm:mb-8 px-3 sm:px-4 py-2 text-xs sm:text-sm font-semibold bg-sky-500 text-white rounded-none shadow-sm w-[120px] text-center">Add Note</Link>
            <div className="w-full overflow-x-auto relative shadow-md lg:max-w-[82rem] ml-auto mr-auto">
                <Table
                    heads={headsTable}
                    data={notes}
                    handleDelete={handleDelete}
                />
            </div>
        </div>
    )
}

export default Notes
