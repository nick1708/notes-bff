import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from "next/server";

export const PATCH = async (req: NextRequest, context: any) => {
    const supabase = createClient();
    const { id } = context.params;
    const note = await req.json();
    console.log('note patch', note)

    const { data: existNote } = await supabase.from('notes').select().eq('id', id).is('deleted_at', false);

    if (!existNote?.length) {
        return NextResponse.json({
            status: 404,
            body: { message: "Note not found" },
        })
    }

    console.log('first', {
      existNote,
      updated_at: new Date().toISOString(),
    })
    const { data, error } = await supabase.from('notes')
        .update({
          ...note,
          updated_at: new Date().toISOString(),
        })
        .eq('id', id)
        .is('deleted_at', false)
        .select();

    if (error || !data.length) {
        return NextResponse.json({
            status: 500,
            body: { message: "Could not update note" },
        });
    }

    return NextResponse.json({ status: 200, data });
}

export const DELETE = async (_request: NextRequest, context: any) => {
    const supabase = createClient();
    const { id } = context.params;
  
    const { data, error } = await supabase.from('notes').update({
      deleted_at: 1,
    }).eq('id', id).select();
  
    if(error) {
      return NextResponse.json({
        status: 500,
        body: { message: "Could not delete note" },
      });
    }
  
    return NextResponse.json({
      status: 200,
      body: { message: "Note deleted successfully!", data },
    });
  }
  
  export const GET = async (_request: NextRequest, context: any) => {
    const supabase = createClient();
    const { id } = context.params;
  
    const { data: existNote } = await supabase.from('notes').select().eq('id', id).is('deleted_at', false).single();
  
    if(!existNote) {
      return NextResponse.json({
        status: 404,
        body: { message: "Note not found" },
      })
    }
  
    return NextResponse.json(existNote);
  }
  