import { Note } from "@/interfaces/INotes";
import { createClient } from "@/utils/supabase/server"
import { NextRequest, NextResponse } from "next/server";

export const GET = async () => {
    const supabase = createClient();

    const { data: notes, error } = await supabase.from('notes').select().is('deleted_at', false);

    if (error) {
        return NextResponse.json({
            status: 400,
            error: error.message
        });
    }

    return NextResponse.json(notes);

}

export const POST = async (req: NextRequest) => {
    const supabase = createClient();
    const user_id = req.headers.get('user_id');
    const note: Note = await req.json();

    if(!user_id)
    return NextResponse.json({ status: 500, message: 'The user does not exist' });

  if(!note.title && !note.content)
    return NextResponse.json({ status: 500, message: 'Invalid note structure' });


    const { data, error } = await supabase.from('notes').insert([{
        title: note.title,
        content: note.content,
        deleted_at: false,   
        user_id: user_id,
    }]).select();

    if (error) {
        return NextResponse.json({
            status: 400,
            error: error.message
        });
    }

    return NextResponse.json(data);
}
