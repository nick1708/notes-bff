import { ITableHead } from "@/components/Table/TableHead";

export const headsTable: ITableHead[] =  [
    {
        title: "ID",
        id: "1"
    },
    {
        title: "Name",
        id: "2"
    },
    {
        title: "Content",
        id: "3"
    },
    {
        title: "Action",
        id: "4"
    }
]