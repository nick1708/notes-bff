'use client';

import { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/navigation';
import Button from '../Button';
import Link from 'next/link';

export interface IForm {
    id?: string;
    className?: string;
    submit(title: string, content: string): void
}

const Form = ({ submit, className = '', id = '' }: IForm) => {
    const route = useRouter();

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    const handlerChangeTitle = (e: any) => {
        const { value } = e.target;
        setTitle(value);
    }

    const handlerChangeContent = (e: any) => {
        const { value } = e.target;
        setContent(value);
    }

    const handleSubmit = (e: any) => {
        e.preventDefault();
        submit(title, content);
        route.push('/notes')
    }
    
    const fetchSingleNote = async () => {
        try {
            const { data, status } = await axios.get(`/api/note/${id}`);
            if (status === 200) {
                setTitle(data.title);
                setContent(data.content);
            }
        } catch (error) {
            console.error("Error fetching single note:", error);
        }
    };
    
    useEffect(() => {
        if (id) fetchSingleNote();
    }, [id]);

    return (
        <form className={`mt-8 space-y-6 max-w-full lg:max-w-[30rem] mx-auto ${className}`}>
            <div className="rounded-md shadow-sm -space-y-px">
                <div className='mb-4 lg:mb-6'>
                    <label htmlFor="title" className="sr-only">Title</label>
                    <input
                        id="title"
                        name="title"
                        type="text"
                        required
                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-sky-500 focus:border-sky-500 focus:z-10 sm:text-sm"
                        placeholder="Title"
                        onChange={handlerChangeTitle}
                        value={title}
                    />
                </div>
                <div className='mb-4 lg:mb-6'>
                    <label htmlFor="content" className="sr-only">Content</label>
                    <textarea
                        id="content"
                        name="content"
                        rows={3}
                        required
                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-sky-500 focus:border-sky-500 focus:z-10 sm:text-sm"
                        placeholder="Content"
                        onChange={handlerChangeContent}
                        value={content}
                    >
                    </textarea>
                </div>
            </div>
            <div className='flex flex-row justify-end gap-4 lg:gap-10'>
                <Button text="Save" onClick={handleSubmit} type="submit" customClass="h-10 lg:h-12 items-center max-w-[5rem] flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-sky-500 hover:bg-sky-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-sky-500" />
                <Link href="/notes" className="h-10 lg:h-12 items-center max-w-[5rem] flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-sky-500 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-sky-500">
                    Cancel
                </Link>
            </div>
        </form>
    )
}

export default Form