interface IButton {
    text: string,
    type: "submit" | "reset" | "button",
    customClass: string,
    onClick: React.MouseEventHandler<HTMLButtonElement> | undefined
}

const Button = ({ text, customClass, type, onClick: onClickProps}: IButton) => {
    return (
        <button onClick={onClickProps} className={customClass} type={type}>
            {text}
        </button>
    );
}

export default Button;
