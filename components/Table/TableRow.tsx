import { Note } from "@/interfaces/INotes";
import Link from "next/link";
import Button from "../Button";

export interface ITableBody {
    data?: Note[];
    handleDelete?: (id: number) => Promise<void>;
  }
  

const TableRow = (props: ITableBody) => {
    const { data, handleDelete } = props;

    const renderNotes = () => {
        if (!data?.length) return;
    
        return data.map((note) => (
          <tr
            key={note.id}
            className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
            <td className="w-4 p-4">{note.id}</td>
            <td
              scope="row"
              className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white text-[14px]">
              {note.title}
            </td>
            <td className="px-6 py-4">{note.content}</td>
            <td className="px-6 py-4 w-[150px] md:w-[250px]">
              <Link href={`/notes/edit/${note.id}`} className="inline-block text-center h-[40px] w-[84px] focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:focus:ring-yellow-900">Edit</Link>
              <Button type="button" customClass="h-[40px] w-[84px] focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900" onClick={() => handleDelete && handleDelete(note.id)} text="Delete" />
            </td>
          </tr>
        ));
      };
    

  return renderNotes();
}

export default TableRow