import { Note } from "@/interfaces/INotes";
import TableHead, { INoteTableHeadProps, ITableHead } from "./TableHead";
import TableRow from "./TableRow";

export interface ITable {
    heads: ITableHead[];
    data: Note[];
    handleDelete?: (id: number) => Promise<void>;
}

const Table = (props: ITable) => {
    const { heads, data = [], handleDelete } = props;
    return (
            <div className="overflow-x-auto">
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <TableHead heads={heads}/>
                    <tbody>
                        <TableRow 
                            data={data} 
                            handleDelete={handleDelete}
                        />
                    </tbody>
                </table>
            </div>
    )
}

export default Table