export interface ITableHead {
  title: string,
  id: string
}

export interface INoteTableHeadProps {
  heads: ITableHead[]
}

const TableHead = (props: INoteTableHeadProps) => {
  const { heads } = props;

  const renderTableHead = () => {
    return heads.map((head) => (
      <th className="p-4" key={head.id}>{head.title}</th>
    ));
  }

  return (
    <thead className="text-xs text-gray-700 uppercase bg-stone-200 dark:bg-gray-700 dark:text-gray-400 sm:table-header">
      <tr>
        {renderTableHead()}
      </tr>
    </thead>

  )
}

export default TableHead